# React Test Project

A minimal development set for react with babel, webpack, css, eslint, flowtype and live-reloading.

## Includes

- React
- Babel
- Webpack
- ESLint
- Browser-Sync
- CSS & style loader
- Flowtype

## Usage

```
npm install    # Install dependencies
npm start    # Run webpack to build then start browser-sync
npm run lint # Lint to src
npm run flow # Check type of annotated sources with `@flow`
```

You can Visit your App once it's startet at http://localhost:3000/
